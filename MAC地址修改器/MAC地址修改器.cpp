// MAC地址修改器.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <windows.h>
#include <time.h>
#include "resource.h"

/************************************************************************/
/* 修改注册表
/************************************************************************/
BOOL DelRegedit(HKEY hKey, PCH str, char* sub_str)
{
    HKEY  m_key;
    DWORD tt = REG_SZ;

    if(RegOpenKeyExA(hKey, str, 0, KEY_ALL_ACCESS | KEY_WOW64_64KEY, &m_key) == ERROR_SUCCESS)
    {
        RegDeleteValueA(m_key, sub_str);		//删除这个值
        RegCloseKey(m_key);
        return TRUE;
    }

    return FALSE;
}
BOOL SetRegedit(HKEY hKey,PCH str,char* sub_str,IN char* xstring)
{
	HKEY  m_key;
	DWORD tt = REG_SZ;

	if( RegCreateKeyA(hKey,str,&m_key) == ERROR_SUCCESS )
	{
		RegSetValueExA(m_key,sub_str,0,tt,( unsigned char *)xstring,lstrlenA(xstring));
		RegCloseKey(m_key);
		return TRUE;
	}

	return FALSE;
}
BOOL GetRegedit(HKEY hKey,PCH str,char* sub_str,OUT char* xstring,DWORD type)
{
	HKEY  m_key;
	DWORD tt = type;
	DWORD ss = MAX_PATH;

	if( RegOpenKeyExA(hKey,str,0,KEY_ALL_ACCESS|KEY_WOW64_64KEY,&m_key) == ERROR_SUCCESS )
	{
		RegQueryValueExA(m_key,sub_str,0,&tt,( unsigned char *)xstring,&ss);
		RegCloseKey(m_key);
		return TRUE;
	}

	return FALSE;
}



/************************************************************************/
/* Mac
/************************************************************************/

const char g_subkeystr[260] = "SYSTEM\\CurrentControlSet\\Control\\Class\\{4D36E972-E325-11CE-BFC1-08002BE10318}";

PCH GetStrToMacStr(__inout PCH macstr)
{
	if(strstr(macstr, "-") == 0)
	{
		char curmac[100] = { 0 };
		for(int x = 0, y = 0; x < strlen(macstr); x++)
		{
			if(x && x % 2 == 0)
				curmac[y++] = '-';

			curmac[y++] = macstr[x];
		}
		lstrcpyA(macstr, curmac);
	}

	return macstr;
}

//ichoice 选中的物理网卡序号
BOOL ChangeMac(int ichoice,PCH macstr)
{
	//遍历所有网卡
	char tmpkey[260], tmp[260];
	for(int i = 0,k=0; i < 100; i++)
	{
		tmp[0] = 0;
		wsprintfA(tmpkey, "%s\\%04d\\Ndi\\Interfaces", g_subkeystr, i);
		GetRegedit(HKEY_LOCAL_MACHINE, tmpkey, "LowerRange", tmp, REG_SZ);
		if(tmp[0] == 0)
			break;

		if(strcmp(tmp, "ethernet") == 0)
		{
			if(k++ == ichoice)
			{
				//设置新的mac数据
				wsprintfA(tmpkey, "%s\\%04d", g_subkeystr, i);
				if(SetRegedit(HKEY_LOCAL_MACHINE, tmpkey, "NetworkAddress", macstr))
				{
					//重启一下网卡
					system("netsh interface set interface name=\"本地连接\" admin=DISABLED");
					system("netsh interface set interface name=\"本地连接\" admin=ENABLED");
					//Win10
					system("netsh interface set interface name=\"以太网\" admin=DISABLED");
					system("netsh interface set interface name=\"以太网\" admin=ENABLED");
					return TRUE;
				}
			}
		}
	}

    return FALSE;
}

//随机一个新的mac
//随机生成MAC地址时，MAC地址16进制中的第一个字节第二个数一定是偶数
VOID GetRandMac(__inout PCH macstr)
{
	srand(time(NULL));
	
	int twobyte[] = {0,2,4,6,8,0xA,0xC,0xE};
	macstr[0] = 0;

	for(int i=0;i<12;i++)
	{
        int ird = rand() % 16;
        //第二个数字是偶数
        if(i == 1)
            ird = twobyte[rand() % 8];
		    
		Sleep(2);
		
		char tmp[3];
		wsprintfA(tmp,"%X",ird);
		lstrcatA(macstr,tmp);
	}
}


void ShowMac(int ichoice,HWND hedit)
{
	//遍历所有网卡
	char tmpkey[260], tmp[260];
	//是否物理网卡
	//HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Class\{4D36E972-E325-11CE-BFC1-08002BE10318}\0007\Ndi\Interfaces   LowerRange == ethernet
	for(int i = 0,k=0; i < 100; i++)
	{
		tmp[0] = 0;
		wsprintfA(tmpkey, "%s\\%04d\\Ndi\\Interfaces", g_subkeystr, i);
		GetRegedit(HKEY_LOCAL_MACHINE, tmpkey, "LowerRange", tmp, REG_SZ);
		if(tmp[0] == 0)
			break;

		if(strcmp(tmp, "ethernet") == 0)
		{
			if(k++ == ichoice)
			{
				char tmpkey[260], tmp[50] = { 0 };
				wsprintfA(tmpkey, "%s\\%04d", g_subkeystr, i);
				GetRegedit(HKEY_LOCAL_MACHINE, tmpkey, "NetworkAddress", tmp, REG_SZ);
				if(tmp[0])
					GetStrToMacStr(tmp);
				SetWindowTextA(hedit, tmp);
				return;
			}
		}
	}
}

VOID InitCurMac(HWND hDlg)
{
	//遍历所有网卡
	char tmpkey[260],tmp[260];
	//是否物理网卡
	//HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Class\{4D36E972-E325-11CE-BFC1-08002BE10318}\0007\Ndi\Interfaces   LowerRange == ethernet
	for(int i=0;i<100;i++)
	{
		tmp[0]=0;
		wsprintfA(tmpkey,"%s\\%04d\\Ndi\\Interfaces", g_subkeystr,i);
		GetRegedit(HKEY_LOCAL_MACHINE,tmpkey,"LowerRange",tmp,REG_SZ);
		if(tmp[0]==0)
			break;

		if(strcmp(tmp,"ethernet")==0)
		{
			wsprintfA(tmpkey,"%s\\%04d", g_subkeystr,i);
			GetRegedit(HKEY_LOCAL_MACHINE,tmpkey,"DriverDesc",tmp,REG_SZ);
			SendDlgItemMessageA(hDlg, IDC_COMBO1, CB_ADDSTRING, 0, (LPARAM)tmp);
		}
	}

	SendDlgItemMessageA(hDlg, IDC_COMBO1, CB_SETCURSEL, 0, 0);
	ShowMac(0, GetDlgItem(hDlg, IDC_EDIT1));
}

/************************************************************************/
/* 运行框Login
/************************************************************************/
BOOL CALLBACK LoginWindowProc(HWND hDlg,UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	switch(uMsg)
	{
	case WM_INITDIALOG:
		InitCurMac(hDlg);
		break;

	case WM_COMMAND:
		if(HIWORD(wParam) == CBN_SELCHANGE)
		{
			int ichoice = SendMessageA(GetDlgItem(hDlg, IDC_COMBO1), CB_GETCURSEL, 0, 0);
			ShowMac(ichoice, GetDlgItem(hDlg, IDC_EDIT1));
			break;
		}

		switch(LOWORD(wParam))
		{
		case IDOK:
			{
				char tmpmac[50] = {0};
				GetDlgItemTextA(hDlg, IDC_EDIT1, tmpmac, 50);
				int ichoice = SendMessageA(GetDlgItem(hDlg, IDC_COMBO1), CB_GETCURSEL, 0, 0);
				if( ChangeMac(ichoice, tmpmac) )
					MessageBoxA(hDlg,"修改完成","",MB_ICONINFORMATION);
			}
			break;

		case IDOK2:
			{
				char tmp[50];
				GetRandMac(tmp);
				GetStrToMacStr(tmp);
				SetDlgItemTextA(hDlg,IDC_EDIT1,tmp);
			}
			break;
		}
		return 1;



	case WM_CLOSE:
		EndDialog(hDlg,0);
		return 1;
	}

	return 0;
}
int WINAPI WinMain( HINSTANCE hInstance,HINSTANCE hPrevInstance, LPSTR lpCmdLine,int nShowCmd )
{
	DialogBoxA(hInstance,MAKEINTRESOURCE(IDD_DIALOG1),(HWND)0,LoginWindowProc);
	return 0;
}
